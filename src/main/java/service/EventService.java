package service;

import model.*;

import java.util.ArrayList;

public class EventService {

    private Device getDeviceById(ArrayList<Device> devices, Integer devId){

        if(devices == null)
            return null;
        for(Device device: devices)
        {
            if(device.getId().equals(devId))
            {
                return device;
            }
        }
        return null;
    }

    public ArrayList<Device> applyEvent(Event event, ArrayList<Device> devices)
    {
        Integer devId;
        Integer compId;
        String type;
        boolean status;

        Device device;

        if(event == null)
            return devices;

        devId = event.getDevId();
        compId = event.getCompId();
        type = event.getType();

        device = getDeviceById(devices,devId);

        if(device == null){
            return devices;
        }

        devices.remove(device);

        status = type.equals("restore");

        device.changeComponentStatus(compId, status);

        devices.add(device);

        return devices;
    }
}
