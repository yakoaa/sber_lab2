import model.*;
import service.EventService;

import java.util.ArrayList;

public class Application {
    public static void main(String[] args) {

        System.out.println("Starting program");

        ArrayList<Component> components1 = new ArrayList<Component>();
        ArrayList<Component> components2 = new ArrayList<Component>();
        ArrayList<Component> components3 = new ArrayList<Component>();
        ArrayList<Component> components4 = new ArrayList<Component>();

        ArrayList<Device> devices = new ArrayList<Device>();

        ArrayList<Event> events = new ArrayList<Event>();

        EventService eventService = new EventService();

        Component comp1 = new Component(1,"comp1", true);
        Component comp2 = new Component(2,"comp2", true);

        components1.add(comp1);
        components1.add(comp2);

        Device device1 = new Device(1, "dev1", "Enabled", components1);

        devices.add(device1);

        Component comp3 = new Component(1,"comp3", true);

        components2.add(comp3);

        Device device2 = new Device(2, "dev2", "Enabled", components2);

        devices.add(device2);

        Component comp4 = new Component(1,"comp4", true);
        Component comp5 = new Component(2,"comp5", true);
        Component comp6 = new Component(3,"comp6", true);

        components3.add(comp4);
        components3.add(comp5);
        components3.add(comp6);

        Device device3 = new Device(3, "dev3", "Enabled", components3);

        devices.add(device3);

        Component comp7 = new Component(1,"comp7", true);
        Component comp8 = new Component(2,"comp8", true);

        components4.add(comp7);
        components4.add(comp8);

        Device device4 = new Device(4, "dev4", "Enabled", components4);

        devices.add(device4);

        ErrorEvent errEvt1 = new ErrorEvent(1,2);
        ErrorEvent errEvt2 = new ErrorEvent(2,1);
        ErrorEvent errEvt3 = new ErrorEvent(4,2);
        ErrorEvent errEvt4 = new ErrorEvent(6,1); //dev not found
        ErrorEvent errEvt5 = new ErrorEvent(4,3); //comp not found

        RestoreEvent resEvt1 = new RestoreEvent(4,2);
        RestoreEvent resEvt2 = new RestoreEvent(2,1);

        events.add(errEvt1);
        events.add(errEvt2);
        events.add(errEvt3);
        events.add(errEvt4);
        events.add(errEvt5);
        events.add(resEvt1);
        events.add(resEvt2);

        for (Event event: events) {
            devices = eventService.applyEvent(event, devices);
        }

        System.out.println("Finish program");
    }
}
