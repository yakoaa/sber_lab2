package model;

public interface Event {

    Integer getDevId();

    void setDevId(Integer devId);

    Integer getCompId();

    void setCompId(Integer compId);

    String getType();
}
