package model;

public class Component {
    private Integer Id;
    private String name;
    private Boolean status;

    public Component(Integer id, String name, Boolean status) {
        Id = id;
        this.name = name;
        this.status = status;
    }

    public Integer getId() {
        return Id;
    }

    public String getName() {
        return name;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
