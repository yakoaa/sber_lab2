package model;

public class ErrorEvent implements Event{

    private Integer devId;
    private Integer compId;
    private final String type = "error";

    public ErrorEvent(Integer devId, Integer compId) {
        this.devId = devId;
        this.compId = compId;
    }

    public Integer getDevId() {
        return devId;
    }

    public void setDevId(Integer devId) {
        this.devId = devId;
    }

    public Integer getCompId() {
        return compId;
    }

    public void setCompId(Integer compId) {
        this.compId = compId;
    }

    public String getType() {
        return type;
    }
}