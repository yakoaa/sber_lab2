package model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class Device {
    private Integer id;
    private String name;
    private String status;
    private ArrayList<Component> components;

    public Device(Integer id, String name, String status, ArrayList<Component> components) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.components = components;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Component> getComponents() {
        return components;
    }

    public void setComponents(ArrayList<Component> components) {
        this.components = components;
    }

    public Component getComponentById(Integer id) {
        if (components == null){
            return null;
        }
        for(Component comp: components) {
            if (comp.getId().equals(id)) {
                //if found
                return comp;
            }
        }
        //if not found
        return null;
    }

    private Component getComponentByStatus(boolean newStatus)
    {
        if (components == null){
            return null;
        }
        for(Component comp: components) {
            if (comp.getStatus().equals(newStatus)) {
                //if found
                return comp;
            }
        }
        //if not found
        return null;
    }

    public void addComponent(Component component) {
        if(component != null)
            components.add(component);
    }

    public boolean changeComponentStatus(Integer compId, boolean newStatus) {

        Logger logger = LoggerFactory.getLogger("Device.class");
        Component comp = this.getComponentById(compId);
        if (comp != null){
            comp.setStatus(newStatus);

            if (this.getComponentByStatus(false) == null) {
                this.setStatus("Normal");
                logger.info(String.format("Устройство Id =  %s,  name = %s изменило свой статус на 'Normal'", this.getId(), this.getStatus()));
            }else{
                if(this.getComponentByStatus(true) != null){
                    this.setStatus("Warning");
                    logger.info(String.format("Устройство Id =  %s,  name = %s изменило свой статус на 'Warning'", this.getId(), this.getStatus()));
                }else{
                    this.setStatus("Error");
                    logger.info(String.format("Устройство Id =  %s,  name = %s изменило свой статус на 'Error'", this.getId(), this.getStatus()));
                }
            }
            return true;
        }
        return false;
    }
}
